SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `blog` ;
CREATE SCHEMA IF NOT EXISTS `blog` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
USE `blog` ;

-- -----------------------------------------------------
-- Table `blog`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `blog`.`users` ;

CREATE TABLE IF NOT EXISTS `blog`.`users` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_level` INT NOT NULL,
  `username` VARCHAR(32) NOT NULL,
  `password` VARCHAR(32) NOT NULL,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  `ip_address` VARCHAR(15) NOT NULL,
  `last_access` DATETIME NOT NULL,
  `created_at` TIMESTAMP NOT NULL,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `blog`.`posts`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `blog`.`posts` ;

CREATE TABLE IF NOT EXISTS `blog`.`posts` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `blog`.`post_contents`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `blog`.`post_contents` ;

CREATE TABLE IF NOT EXISTS `blog`.`post_contents` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `post` INT NOT NULL,
  `lang` VARCHAR(45) NOT NULL,
  `author` INT NOT NULL,
  `article` TEXT NOT NULL,
  `created_at` TIMESTAMP NOT NULL,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_post_contents_users1_idx` (`author` ASC),
  INDEX `fk_post_contents_posts1_idx` (`post` ASC),
  UNIQUE INDEX `post_lang_UNIQUE` (`post` ASC, `lang` ASC),
  CONSTRAINT `fk_post_contents_users1`
    FOREIGN KEY (`author`)
    REFERENCES `blog`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_post_contents_posts1`
    FOREIGN KEY (`post`)
    REFERENCES `blog`.`posts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `blog`.`comments`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `blog`.`comments` ;

CREATE TABLE IF NOT EXISTS `blog`.`comments` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `post` INT NOT NULL,
  `commentor` INT NOT NULL,
  `text` TEXT NOT NULL,
  `created_at` TIMESTAMP NOT NULL,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_comments_users1_idx` (`commentor` ASC),
  INDEX `fk_comments_post_contents2_idx` (`post` ASC),
  CONSTRAINT `fk_comments_users1`
    FOREIGN KEY (`commentor`)
    REFERENCES `blog`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_comments_post_contents2`
    FOREIGN KEY (`post`)
    REFERENCES `blog`.`post_contents` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `blog`.`tags`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `blog`.`tags` ;

CREATE TABLE IF NOT EXISTS `blog`.`tags` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `tag` VARCHAR(25) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `tag_UNIQUE` (`tag` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `blog`.`post_tags`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `blog`.`post_tags` ;

CREATE TABLE IF NOT EXISTS `blog`.`post_tags` (
  `post` INT UNSIGNED NOT NULL,
  `tags_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`post`, `tags_id`),
  INDEX `fk_tags_posts1_idx` (`post` ASC),
  INDEX `fk_post_tags_tags2_idx` (`tags_id` ASC),
  CONSTRAINT `fk_tags_posts1`
    FOREIGN KEY (`post`)
    REFERENCES `blog`.`posts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_post_tags_tags2`
    FOREIGN KEY (`tags_id`)
    REFERENCES `blog`.`tags` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `blog`.`related_posts`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `blog`.`related_posts` ;

CREATE TABLE IF NOT EXISTS `blog`.`related_posts` (
  `post_id1` INT UNSIGNED NOT NULL,
  `post_id2` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`post_id1`, `post_id2`),
  INDEX `fk_posts_has_posts_posts1_idx` (`post_id2` ASC),
  INDEX `fk_posts_has_posts_posts_idx` (`post_id1` ASC),
  CONSTRAINT `fk_posts_has_posts_posts`
    FOREIGN KEY (`post_id1`)
    REFERENCES `blog`.`posts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_posts_has_posts_posts1`
    FOREIGN KEY (`post_id2`)
    REFERENCES `blog`.`posts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE = '';
GRANT USAGE ON *.* TO blog;
 DROP USER blog;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
CREATE USER 'blog' IDENTIFIED BY 'blog';

GRANT ALL ON `blog`.* TO 'blog';

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
