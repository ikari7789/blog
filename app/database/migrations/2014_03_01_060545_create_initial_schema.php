<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInitialSchema extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($table)
		{
			$table->engine = 'InnoDB';

			$table->increments('id')
				->unsigned();
			$table->integer('user_level');
			$table->string('username', 32)
				->unique();
			$table->string('password', 32);
			$table->string('first_name', 45);
			$table->string('last_name', 45);
			$table->string('email', 100)
				->unique();
			$table->string('ip_address', 15);
			$table->dateTime('last_access');
			$table->timestamps();
		});

		Schema::create('posts', function($table)
		{
			$table->engine = 'InnoDB';

			$table->increments('id')
				->unsigned();
		});

		Schema::create('post_contents', function($table)
		{
			$table->engine = 'InnoDB';

			$table->increments('id')
				->unsigned();
			$table->integer('post')
				->unsigned();
			$table->string('lang', 45);
			$table->integer('author')
				->unsigned();
			$table->text('article');
			$table->timestamps();

			$table->unique(array('post', 'lang'));

			$table->foreign('post')
				->references('id')
				->on('post_contents');
			$table->foreign('author')
				->references('id')
				->on('users');
		});

		Schema::create('comments', function($table)
		{
			$table->engine = 'InnoDB';

			$table->increments('id')
				->unsigned();
			$table->integer('post')
				->unsigned();
			$table->integer('commentor')
				->unsigned();
			$table->text('text');
			$table->timestamps();

			$table->foreign('post')
				->references('id')
				->on('post_contents');
			$table->foreign('commentor')
				->references('id')
				->on('users');
		});

		Schema::create('tags', function($table)
		{
			$table->engine = 'InnoDB';

			$table->increments('id')
				->unsigned();
			$table->string('tag', 25)
				->unique();
		});

		Schema::create('post_tags', function($table)
		{
			$table->engine = 'InnoDB';

			$table->integer('post')
				->unsigned();
			$table->integer('tag')
				->unsigned();

			$table->primary(array('post', 'tag'));

			$table->foreign('post')
				->references('id')
				->on('posts');
			$table->foreign('tag')
				->references('id')
				->on('tags');
		});

		Schema::create('related_posts', function($table)
		{
			$table->engine = 'InnoDB';

			$table->integer('post_id1')
				->unsigned();
			$table->integer('post_id2')
				->unsigned();

			$table->primary(array('post_id1', 'post_id2'));

			$table->foreign('post_id1')
				->references('id')
				->on('posts');
			$table->foreign('post_id2')
				->references('id')
				->on('posts');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
		Schema::drop('posts');
		Schema::drop('post_contents');
		Schema::drop('comments');
		Schema::drop('tags');
		Schema::drop('post_tags');
		Schema::drop('related_posts');
	}

}
